import java.util.Scanner;
import fileIO.*;

public class Program{

    public static void main(String[] args){
        MainMenu();
    }

    public static void MainMenu(){
        System.out.println("Choose from these choices");
        System.out.println("--------------------\n");
        System.out.println("1: List all files");
        System.out.println("2: Get specific files depending on an extension");
        System.out.println("3: Fetch specific information on Dracula");
        System.out.println("9: Exit \n");

        FileManipulation files = new FileManipulation();
        Scanner inputReader = new Scanner(System.in);
        int selection = inputReader.nextInt();
        String input;
        
            switch (selection) {
                case 1:
                    // List all the files in the folder
                    files.ListAllFiles();
                    MainMenu();
                    break;

                case 2:
                    //List all files with a given extension
                    System.out.println("Enter one of the following extensions: .txt , .jpeg , .jfif , .png or .jpg: ");

                    // Read the input from the terminal, and pass it on as a parameter for the method call     
                    input = inputReader.nextLine();     
                    
                    System.out.println("\nYou have entered: " + input);  
                    files.ListFilesFromExtension(input);
                    MainMenu();
                    break;

                case 3:
                    // Runs the menu for file manipulation
                    FileManipulationMenu();
                default:
                System.out.println("Thank you for using the program");
            }
        }
    

    public static void FileManipulationMenu(){
        //Menu for File manipulation
        System.out.println("\nWelcome to file manipulation of Dracula.txt");
        System.out.println("Please choose between the following options");
        System.out.println("--------------------\n");
        System.out.println("1: Get name and size of file");
        System.out.println("2: Get the amount of lines in the file");
        System.out.println("3: Search for how many times a word is found within the file");
        System.out.println("9: Return to the main menu");

        FileManipulation files = new FileManipulation();
        Scanner inputReader = new Scanner(System.in);
        int selection = inputReader.nextInt();

        switch(selection) {
            case 1:
                // Gets the name and size of the file
                files.GetNameAndSizeOfFile();  
                FileManipulationMenu();       
                break;

            case 2:
                // Get the amount of lines in the file
                files.GetAmountOfLines();
                FileManipulationMenu();
                break;

            case 3:
                // Search for specific word in the file
                files.SearchForSpecificWord();
                FileManipulationMenu();
                break;

            default:
            // Returns to the main menu
            MainMenu();

        }
    }
}