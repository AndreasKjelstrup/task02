package fileIO;

import java.io.*;
import java.util.*;
import java.text.*;

public class FileManipulation{

    // Variables used for this solution
    final static File folderPath = new File("../../FileIOProject/Files");
    static DecimalFormat df = new DecimalFormat("#.##");
    static String output = "";
    static String logMessage = "";
    static long startTime;
    static long endTime;
    static SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyy HH:mm:ss");
    static Date date = new Date();

    // Function for listing all the files in the given folder
    public static void ListAllFiles(){
        Scanner inputReader = new Scanner(System.in);
        startTime = System.currentTimeMillis();
        File[] listOfFiles = folderPath.listFiles();

        // Iterate through the array of files found, and print out their filenames
        for(int i = 0; i < listOfFiles.length; i++){
            if(listOfFiles[i].isFile()){
                System.out.println("File: " + listOfFiles[i].getName());
            }
        }
        endTime = System.currentTimeMillis();
        // Create a log message with the amount of time it took to finish the function, and log the message in log.txt
        logMessage = "The operation of listing all files took: " + (endTime - startTime) + "ms";
        LogMessage(logMessage);
        System.out.println(logMessage);

        System.out.println("Press any key to return to the main menu");
        String input = inputReader.nextLine();
    }

    // List all files from a given extension
    public static void ListFilesFromExtension(String chosenExtension){

        Scanner inputReader = new Scanner(System.in);
        // Creates a filter for checking through the extensions, returning true if the chosen extension corresponds with a file.
        FilenameFilter fileFilter = new FilenameFilter(){
            public boolean accept(File dir, String name) {
                name = name.toLowerCase();
                if(name.endsWith(chosenExtension)) {
                    return true;
                } else{
                    return false;
                }
            }
        };

        startTime = System.currentTimeMillis();
        // Lists all the files with a given extension
        String[] filesFoundList = folderPath.list(fileFilter);

        System.out.println("List of files found with the following extension" + chosenExtension);
        // Iterates and prints all of the files with the matched extension
        for(String fileName : filesFoundList){
            System.out.println(fileName);
        }

        endTime = System.currentTimeMillis();
        System.out.println("\n");

        // Create a log message with the amount of time it took to finish the function, and log the message in log.txt
        logMessage = "The operation of listing all files with a given extension took: " + (endTime - startTime) + "ms";
        LogMessage(logMessage);
        System.out.println(logMessage);

        System.out.println("Press any key to return to the main menu");
        String input = inputReader.nextLine();
    }

    // Function for getting the name of the textfile
    public static void GetNameAndSizeOfFile(){           

        Scanner inputReader = new Scanner(System.in);

        // Open a filereader with a given filepath
        try(FileInputStream fileInputStream = new FileInputStream(folderPath + "/Dracula.txt")){
            int data = fileInputStream.read();
            int byteCount = 0;
            startTime = System.currentTimeMillis();
            
            // Read and count all bytes of the file
            while (data != -1) {

                byteCount++;
                data = fileInputStream.read();
            }

            // Calculate the counted bytes in to kilobytes
            double kilobytes = byteCount/1024.0;
            endTime = System.currentTimeMillis();

            System.out.println("\nThe file named Dracula.txt is: " + byteCount + "bytes, or " + df.format(kilobytes) + " kilobytes" + "\n");
        }
        catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

        // Create a log message with the amount of time it took to finish the function, and log the message in log.txt        
        logMessage = "The operation of getting the name and the size of Dracula.txt took: " + (endTime - startTime) + "ms";
        LogMessage(logMessage);
        System.out.println(logMessage);

        System.out.println("Press any key to return to the main menu");
        String input = inputReader.nextLine();
    }

    // Function for getting the amount of lines in the textfile
    public static void GetAmountOfLines(){

        int amountOfLines = 0;       
        Scanner inputReader = new Scanner(System.in);
        
        // Open a filereader with a given filepath
        try(BufferedReader reader = new BufferedReader(new FileReader(folderPath + "/Dracula.txt"))){

            startTime = System.currentTimeMillis();

            // Read each line and increment amountOfLines every time a line has been read
            while((reader.readLine()) != null){
                amountOfLines++;
            }
            endTime = System.currentTimeMillis();
        } catch (IOException ex){
            System.out.println(ex.getMessage());
        } 
        System.out.println("\nThe file Dracula.txt has: " + amountOfLines + " lines");

        // Create a log message with the amount of time it took to finish the function, and log the message in log.txt
        logMessage = "The operation of getting the amount of lines in Dracula.txt took: " + (endTime - startTime) + "ms";
        LogMessage(logMessage);
        System.out.println(logMessage);

        System.out.println("Press any key to return to the main menu");
        String input = inputReader.nextLine();
    }

    // Function for finding occurences of a word that the user inputs
    public static void SearchForSpecificWord(){

        System.out.println("Please provide the word you want searched for in the textfile" + "\n");

        // Create a new scanner that reads the word-input from the user
        Scanner inputReader = new Scanner(System.in);
        String input = inputReader.nextLine();
        input = input.toLowerCase();

        // Open a filereader with a given filepath
        try(BufferedReader reader = new BufferedReader(new FileReader(folderPath + "/Dracula.txt"))){
            
            int amountOfFilteredLines = 0;
            String line;
            String[] words;
            startTime = System.currentTimeMillis();

            // Iterate through the textfile and find occurances of the word-input.
            while((line = reader.readLine()) != null){

                line = line.toLowerCase();
                line = line.replaceAll("[,.]", "");
                words = line.split(" ");

                for(int i = 0; i < words.length; i++){
                    words[i].trim();
                    if(words[i].equals(input)){
                        amountOfFilteredLines++;
                    }
                }
            }
            endTime = System.currentTimeMillis();
            System.out.println("The word you entered was: " + input + "\n");

            // Error handling if the word wasn't found within the file
            if(amountOfFilteredLines == 0){
                System.out.println("The word you were searching for can't be found in this file, please try again" + "\n");
                SearchForSpecificWord();
            } else if(amountOfFilteredLines != 0){
                System.out.println("The word: '" + input + "' was found : " + amountOfFilteredLines + " times");
            }
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }

        // Create a log message with the amount of time it took to finish the function, and log the message in log.txt
        logMessage = "The operation of finding the occurences of the word '" + input + "' in the file dracula.txt took: " + (endTime - startTime) + "ms";
        LogMessage(logMessage);
        System.out.println(logMessage);  

        System.out.println("Press any key to return to the main menu");
        input = inputReader.nextLine();
    }

    // Function for logging information to the textfile
    public static void LogMessage(String logMessage) {
 
        // Create a new file if the file isn't created already
        File logFile = new File("log.txt");
        try(FileOutputStream fos = new FileOutputStream(logFile, true)){ 
        
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(fos));
        
        // Formatting for the log message, making it look better, before it's written to the file, and the writer is closed 
        logMessage = "[" + formatter.format(date) + "]" + " - " + logMessage;
        writer.write(logMessage);
        writer.newLine();
        writer.close(); 
        }
        catch (IOException ex){

            ex.getMessage();
        }

    }
}
