#  Readme
### This project is about manipulating files (fetching file info, listing files from extensions etc.)
### This project contains a generic filepath, meaning you won't have to copypaste your own path.

Pictures provided for the solution: 
![image1](/Pictures/Image01.png)

![image2](/Pictures/Image02.png)

![image3](/Pictures/Image03.png)